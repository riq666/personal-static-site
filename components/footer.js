import { Box } from '@chakra-ui/react'

const Footer = () => {
  return (
    <Box align="center" opacity={0.4} fontSize="sm">
      &copy; {new Date().getFullYear()} Mikail Thoriq. | 3D dog from github.com/craftzdog/voxel-dog
    </Box>
  )
}

export default Footer
