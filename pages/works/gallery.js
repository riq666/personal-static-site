import {
  Container,
  Badge,
  Link,
  List,
  ListItem
} from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Gallery">
    <Container>
      <Title>
       Gallery <Badge>2010</Badge>
      </Title>
      <P>
      Robust gallery website with intuitive UI
      </P>
      <List ml={4} my={4}>
      <Meta>Website</Meta>
          <Link href="https://gallery.mikailthoriq.my.id">
          https://gallery.mikailthoriq.my.id <ExternalLinkIcon mx="2px" />
          </Link>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTML, CSS, JavaScript</span>
        </ListItem>
      </List>
      <WorkImage src="/images/works/galleryhomepage1.png" alt="homepagegallery" />
    </Container>
  </Layout>
)

export default Work
