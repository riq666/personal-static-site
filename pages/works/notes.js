import {
  Container,
  Badge,
  Link,
  List,
  ListItem
} from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Notes">
    <Container>
      <Title>
        Notes <Badge>2020-</Badge>
      </Title>
      <P>
       You&apos;ll find Notes of all my experiment during pandemic
      </P>

      <List ml={4} my={4}>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTML 5, CSS, JavaScript, PHP</span>
        </ListItem>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://wiki.mikailthoriq.my.id/notes">
          https://wiki.mikailthoriq.my.id/notes <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
      </List>
      <WorkImage src="/images/works/noteshomepage1.png" alt="noteshomepage" />
    </Container>
  </Layout>
)

export default Work
