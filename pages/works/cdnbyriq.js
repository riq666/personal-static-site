import { Container, Badge, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="cdnbyriq">
    <Container>
      <Title>
        CDN By Riq. <Badge>2020-</Badge>
      </Title>
      <P>
      A storage system that optimzed for speed and reliability and faster load time in a website
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://riqq-cdn.cf/home">
          https://riqq-cdn.cf/home <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTML 5, CSS, JavaScript</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/cdnbyriq1.png" alt="Inkdrop" />
    </Container>
  </Layout>
)

export default Work
