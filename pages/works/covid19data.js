import { Container, Badge, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="C19 Tracker">
    <Container>
      <Title>
      Corona Virus Data Tracker <Badge>2021-</Badge>
      </Title>
      <P>
      Simple Corona Virus Data Tracker (little bit buggy rn..)
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Website</Meta>
          <Link href="https://c19tracker.mikailthoriq.ml/">
          https://c19tracker.mikailthoriq.ml/ <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTMl, CSS, REACT, JAVASCRIPT</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/homec19tracker.png" alt="c19homepage" />
    </Container>
  </Layout>
)

export default Work
