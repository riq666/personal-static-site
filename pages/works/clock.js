import {
  Container,
  Badge,
  Link,
  List,
  ListItem
} from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Clock">
    <Container>
      <Title>
        Clock <Badge>late 2021-</Badge>
      </Title>
      <P>
        Just a web app that shows the time.
      </P>
      <List ml={4} my={4}>
      <Meta>Website</Meta>
          <Link href="https://clock.byriq.cf">
          https://clock.byriq.cf <ExternalLinkIcon mx="2px" />
          </Link>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTML, CSS, JavaScript</span>
        </ListItem>
      </List>
      <WorkImage src="/images/works/analogclock1.png" alt="analogclock" />
      <WorkImage src="/images/works/rgbclock.png" alt="digitalclock" />
    </Container>
  </Layout>
)

export default Work
