import {
  Container,
  Badge,
  Link,
  List,
  ListItem
} from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="Lab">
    <Container>
      <Title>
        Lab <Badge>2020-</Badge>
      </Title>
      <P>Riq&apos;s Web Lab is a collection of different project like php.</P>
      <List ml={4} my={4}>
      <Meta>Website</Meta>
          <Link href="https://lab.mikailthoriq.my.id">
          https://lab.mikailthoriq.my.id<ExternalLinkIcon mx="2px" />
          </Link>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Web</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>HTML 5, CSS, JavaScript, PHP</span>
        </ListItem>
      </List>

      <WorkImage src="/images/works/labhome1.png" alt="labhomepage" />
    </Container>
  </Layout>
)

export default Work
