import { Container, Heading, SimpleGrid } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { GridItem } from '../components/grid-item'

import thumbbestarch from '../public/images/contents/arch.jpg'
import thumbhelloworld from '../public/images/contents/C-lang-ss_hello.png'
import thumbfreehosting from '../public/images/contents/freehosting.png'
import thumbanicli from '../public/images/contents/ani-cli.png'

const Posts = () => (
  <Layout title="Posts">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Some Posts
      </Heading>

      <Section delay={0.1}>
        <SimpleGrid columns={[1, 2, 2]} gap={6}>
          <GridItem
            title="Best Archlinux Based Distros"
            thumbnail={thumbbestarch}
            href="https://blog.mikailthoriq.ml/posts/best-archlinux-for-me"
            target="_blank"
          />
          <GridItem
            title="Hello World"
            thumbnail={thumbhelloworld}
            href="https://blog.mikailthoriq.ml/posts/hello-world"
            target="_blank"
          />
          <GridItem
          title="Free Hosting With Cpanel"
          thumbnail={thumbfreehosting}
          href="https://blog.mikailthoriq.ml/posts/best-free-hosting-with-cpanel"
          target="_blank"
          />
          <GridItem 
          title="Watch Anime With CLI (Linux)"
          thumbnail={thumbanicli}
          href="https://blog.mikailthoriq.ml/posts/watch-anime-with-cli"
          target="_blank"
          />
        </SimpleGrid>
      </Section>
    </Container>
  </Layout>
)

export default Posts
