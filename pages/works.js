import { Container, Heading, SimpleGrid, Divider } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { WorkGridItem } from '../components/grid-item'

import thumbcdnbyriq from '../public/images/works/cloud.png'
import thumblab from '../public/images/works/code.png'
import thumbnotes from '../public/images/works/pencil.png'
import thumbc19 from '../public/images/works/coronavirus.png'
import thumbgallery from '../public/images/works/gallery.png'
import thumbosint from '../public/images/works/42422997.png'
import thumbclock from '../public/images/works/clock.png'

const Works = () => (
  <Layout title="Works">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Content Delivery Projects
      </Heading>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section>
          <WorkGridItem id="cdnbyriq" title="cdnbyriq" thumbnail={thumbcdnbyriq}>
            A storage system that optimzed for speed and reliability and faster load time in a website
          </WorkGridItem>
        </Section>
      </SimpleGrid>

      <Section delay={0.2}>
        <Divider my={6} />

        <Heading as="h3" fontSize={20} mb={4}>
          Experiment
        </Heading>
      </Section>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section delay={0.3}>
          <WorkGridItem
            id="lab"
            thumbnail={thumblab}
            title="Lab"
          >
           Riq&apos;s Web Lab
          </WorkGridItem>
        </Section>
        <Section delay={0.3}>
          <WorkGridItem id="notes" thumbnail={thumbnotes} title="Notes">
          Notes of all experiment
          </WorkGridItem>
        </Section>
        <Section delay={0.3}>
          <WorkGridItem id="covid19data" thumbnail={thumbc19} title="Covid 19 Data Tracker">
          Simple Corona Virus Data Tracker
          </WorkGridItem>
        </Section>
      </SimpleGrid>

      <Section delay={0.4}>
        <Divider my={6} />

        <Heading as="h3" fontSize={20} mb={4}>
          Random
        </Heading>
      </Section>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section delay={0.5}>
          <WorkGridItem id="gallery" thumbnail={thumbgallery} title="Gallery">
          Robust gallery website with intuitive UI
          </WorkGridItem>
        </Section>
        <Section delay={0.5}>
          <WorkGridItem
            id="osint"
            thumbnail={thumbosint}
            title="OSINT"
          >
            OSINT framework focused on gathering information from free tools or resources.
          </WorkGridItem>
        </Section>
        <Section delay={0.6}>
          <WorkGridItem id="clock" thumbnail={thumbclock} title="Clock">
            Just Clocks
          </WorkGridItem>
        </Section>
      </SimpleGrid>
    </Container>
  </Layout>
)

export default Works
